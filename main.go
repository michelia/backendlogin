package main

import (
	"fmt"
	"os"

	"github.com/Sirupsen/logrus"
	"github.com/aiwantaozi/backendlogin/server"
	"github.com/urfave/cli"

	_ "github.com/aiwantaozi/backendlogin/server/harbor"
	_ "github.com/aiwantaozi/backendlogin/server/rancher"
)

func main() {
	app := cli.NewApp()
	app.Name = "proxy-server"
	app.Usage = "Start the proxy server to hack login"
	app.Action = launch

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "cattle-access-key",
			Usage:  "Cattle API Access Key",
			EnvVar: "CATTLE_ACCESS_KEY",
		},
		cli.StringFlag{
			Name:   "cattle-access-secret",
			Usage:  "Cattle API Secret Key",
			EnvVar: "CATTLE_SECRET_KEY",
		},
		cli.StringFlag{
			Name:   "harbor-admin-name",
			Usage:  "Harbor admin name",
			EnvVar: "HARBOR_ADMIN_NAME",
		},
		cli.StringFlag{
			Name:   "harbor-admin-password",
			Usage:  "harbor admin password",
			EnvVar: "HARBOR_ADMIN_PASSWORD",
		},
		cli.StringFlag{
			Name:   "backend-url",
			Usage:  "backend url",
			EnvVar: "BACKEND_URL",
		},
		cli.StringFlag{
			Name:   "common-password",
			Usage:  "common password",
			EnvVar: "COMMON_PASSWORD",
		},
		cli.StringFlag{
			Name:   "server-type",
			Usage:  "Start Harbor proxy or Rancher proxy",
			EnvVar: "SERVER_TYPE",
		},
		cli.StringFlag{
			Name:   "redirect-url-on-fail",
			Usage:  "redirect url on fail",
			EnvVar: "REDIRECT_URL_ON_FAIL",
		},
		cli.StringFlag{
			Name:   "url-after-proxy",
			Usage:  "url after proxy",
			EnvVar: "URL_AFTER_PROXY",
		},
		cli.StringFlag{
			Name:   "listen",
			Usage:  "server port",
			Value:  "8091",
			EnvVar: "LISTEN",
		},
	}

	app.Run(os.Args)
}

func launch(c *cli.Context) error {
	serverType := c.String("server-type")
	logrus.Infof("starting proxy backend %s", serverType)
	s, err := server.GetServer(serverType, c)
	if err != nil {
		return err
	}
	port := fmt.Sprintf(":%s", c.String("listen"))
	logrus.Infof("Server start at port %s", port)
	s.StartServer(port)
	return nil
}
