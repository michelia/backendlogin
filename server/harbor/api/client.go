package api

import (
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

type HarborAPIClient struct {
	harborURL           string
	harborAdminName     string
	harborAdminPassword string
}

type User struct {
	UserID       int       `json:"user_id"`
	Username     string    `json:"username"`
	Email        string    `json:"email"`
	Password     string    `json:"password"`
	Realname     string    `json:"realname"`
	Comment      string    `json:"comment"`
	Deleted      int       `json:"deleted"`
	RoleName     string    `json:"role_name"`
	RoleID       int       `json:"role_id"`
	HasAdminRole int       `json:"has_admin_role"`
	ResetUUID    string    `json:"reset_uuid"`
	CreationTime time.Time `json:"creation_time"`
	UpdateTime   time.Time `json:"update_time"`
}

func NewHarborClient(harborURL string, harborAdminName string, harborAdminPassword string) *HarborAPIClient {
	return &HarborAPIClient{
		harborURL:           harborURL,
		harborAdminName:     harborAdminName,
		harborAdminPassword: harborAdminPassword,
	}
}

func (h *HarborAPIClient) GetAccount() ([]User, error) {
	reqURL := h.harborURL + "/api/users"
	req, err := http.NewRequest("GET", reqURL, nil)
	req.Header.Add("Authorization", "Basic "+basicAuth(h.harborAdminName, h.harborAdminPassword))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	userList := []User{}
	err = json.Unmarshal(body, &userList)
	if err != nil {
		return nil, err
	}
	return userList, nil
}

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}
