package harbor

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/Sirupsen/logrus"
	"github.com/aiwantaozi/backendlogin/server"
	"github.com/aiwantaozi/backendlogin/server/harbor/api"
	"github.com/aiwantaozi/backendlogin/utils"
	"github.com/urfave/cli"
)

type Server struct {
	HarborURL           string
	HarborAdminName     string
	HarborAdminPassword string
	RedirectURLOnFail   string
	URLAfterProxy       string
	CommonPassword      string
	harborAPIClient     *api.HarborAPIClient
}

func init() {
	server.RegisterServer("harbor", &Server{})
}

func (r *Server) GetName() string {
	return "harbor"
}

func (r *Server) Init(c *cli.Context) (err error) {
	r.URLAfterProxy = c.String("url-after-proxy")
	r.CommonPassword = c.String("common-password")
	r.RedirectURLOnFail = c.String("redirect-url-on-fail")
	r.HarborURL = c.String("backend-url")
	if c.String("backend-url") == "" || c.String("harbor-admin-name") == "" || c.String("harbor-admin-password") == "" || r.CommonPassword == "" || r.URLAfterProxy == "" || r.RedirectURLOnFail == "" {
		logrus.Error("no harbor info")
		return fmt.Errorf("please input harbor info")
	}
	r.harborAPIClient = api.NewHarborClient(c.String("backend-url"), c.String("harbor-admin-name"), c.String("harbor-admin-password"))
	return nil
}

func (r *Server) SetCookieHandler(w http.ResponseWriter, req *http.Request) {
	userName := req.URL.Query().Get("userName")
	if userName == "" {
		logrus.Error("userName is empty")
		http.Redirect(w, req, r.RedirectURLOnFail, http.StatusTemporaryRedirect)
		return
	}

	logrus.Infof("userName is %s", userName)
	err := r.CheckUser(userName)
	if err != nil {
		logrus.Error("fail check user", err)
		http.Redirect(w, req, r.RedirectURLOnFail, http.StatusTemporaryRedirect)
		return
	}

	cookies, err := r.GetCookies(userName)
	if err != nil {
		logrus.Error("get cookies error", err)
		http.Redirect(w, req, r.RedirectURLOnFail, http.StatusTemporaryRedirect)
		return
	}
	r.SetCookies(w, cookies)
	http.Redirect(w, req, r.URLAfterProxy, http.StatusTemporaryRedirect)
}

func (r *Server) StartServer(port string) {
	logrus.Info("start harbor proxy")
	http.HandleFunc("/myharbor", http.HandlerFunc(r.SetCookieHandler))
	http.ListenAndServe(port, nil)
}

func (r *Server) CheckUser(userName string) error {
	accountList, err := r.harborAPIClient.GetAccount()
	if err != nil {
		return err
	}
	for _, v := range accountList {
		if v.Username == userName {
			return nil
		}
	}
	return fmt.Errorf("could not find user: %s", userName)
}

func (r *Server) GetCookies(username string) (setCookie []*http.Cookie, err error) {
	reqURL := r.HarborURL + "/login"

	data := url.Values{}
	data.Set("principal", username)
	data.Add("password", r.CommonPassword)

	req, _ := http.NewRequest("POST", reqURL, strings.NewReader(data.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}

	setCookie = utils.ReadSetCookies(res.Header)

	defer res.Body.Close()
	return
}

func (r *Server) SetCookies(writer http.ResponseWriter, cookies []*http.Cookie) {
	for _, v := range cookies {
		http.SetCookie(writer, v)
	}
}
