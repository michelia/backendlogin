package server

import (
	"fmt"
	"net/http"

	"github.com/Sirupsen/logrus"
	"github.com/urfave/cli"
)

var (
	servers = make(map[string]Server)
)

type Server interface {
	GetName() string
	Init(c *cli.Context) error
	StartServer(port string)
	CheckUser(userName string) error
	GetCookies(username string) ([]*http.Cookie, error)
	SetCookies(w http.ResponseWriter, c []*http.Cookie)
}

func GetServer(name string, c *cli.Context) (Server, error) {
	if server, ok := servers[name]; ok {
		if err := server.Init(c); err != nil {
			return nil, err
		}
		return server, nil
	}
	return nil, fmt.Errorf("No such server '%s'", name)
}

func RegisterServer(name string, server Server) {
	if _, exists := servers[name]; exists {
		logrus.Fatalf("Provider '%s' tried to register twice", name)
	}
	servers[name] = server
}
