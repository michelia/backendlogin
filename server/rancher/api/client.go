package api

import (
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	rancher "github.com/rancher/go-rancher/v2"
)

const ClientTimeout = 60 * time.Second

type RancherAPIClient struct {
	RancherClient *rancher.RancherClient
}

func NewRancherClient(url string, accessKey string, secretKey string) (*RancherAPIClient, error) {

	rancherClient, err := rancher.NewRancherClient(&rancher.ClientOpts{
		Url:       url,
		AccessKey: accessKey,
		SecretKey: secretKey,
		Timeout:   ClientTimeout,
	})
	if err != nil {
		logrus.Errorf("fail create rancher client: %v", err)
		return nil, err
	}
	return &RancherAPIClient{RancherClient: rancherClient}, nil
}

func (r *RancherAPIClient) GetAccount() ([]rancher.Account, error) {
	userFilter := map[string]interface{}{
		"state": "active",
		"kind":  "user",
	}
	userList, err := r.RancherClient.Account.List(&rancher.ListOpts{Filters: userFilter})
	if err != nil {
		return []rancher.Account{}, errors.Wrap(err, "fail get rancher account list")
	}

	adminFilter := map[string]interface{}{
		"state": "active",
		"kind":  "admin",
	}
	adminList, err := r.RancherClient.Account.List(&rancher.ListOpts{Filters: adminFilter})
	if err != nil {
		return []rancher.Account{}, errors.Wrap(err, "fail get rancher account list")
	}

	totalUser := append(adminList.Data, userList.Data...)
	return totalUser, nil
}
