package rancher

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/Sirupsen/logrus"
	"github.com/aiwantaozi/backendlogin/server"
	"github.com/aiwantaozi/backendlogin/server/rancher/api"
	"github.com/urfave/cli"
)

type Server struct {
	CattleTokenURL    string
	RedirectURLOnFail string
	URLAfterProxy     string
	CommonPassword    string
	rancherAPIClient  *api.RancherAPIClient
}

type Login struct {
	Code         string `json:"code"`
	AuthProvider string `json:"authProvider"`
}

type AuthResponse struct {
	ID    interface{} `json:"id"`
	Type  string      `json:"type"`
	Links struct {
	} `json:"links"`
	BaseType    string `json:"baseType"`
	ActionLinks struct {
	} `json:"actionLinks"`
	AccountID     string       `json:"accountId"`
	AuthProvider  string       `json:"authProvider"`
	Code          interface{}  `json:"code"`
	Enabled       bool         `json:"enabled"`
	Jwt           string       `json:"jwt"`
	OriginalLogin interface{}  `json:"originalLogin"`
	RedirectURL   interface{}  `json:"redirectUrl"`
	Security      bool         `json:"security"`
	User          string       `json:"user"`
	UserIdentity  UserIdentity `json:"userIdentity"`
	UserType      string       `json:"userType"`
}

type UserIdentity struct {
	ExternalID     string      `json:"externalId"`
	ProfilePicture interface{} `json:"profilePicture"`
	Name           string      `json:"name"`
	ExternalIDType string      `json:"externalIdType"`
	ProfileURL     interface{} `json:"profileUrl"`
	Login          string      `json:"login"`
	Role           interface{} `json:"role"`
	ProjectID      interface{} `json:"projectId"`
	User           bool        `json:"user"`
	All            interface{} `json:"all"`
	ID             string      `json:"id"`
}

func init() {
	server.RegisterServer("rancher", &Server{})
}

func (r *Server) GetName() string {
	return "rancher"
}

func (r *Server) Init(c *cli.Context) (err error) {
	r.URLAfterProxy = c.String("url-after-proxy")
	r.CommonPassword = c.String("common-password")
	r.RedirectURLOnFail = c.String("redirect-url-on-fail")
	if c.String("cattle-access-key") == "" || c.String("cattle-access-secret") == "" || c.String("backend-url") == "" || r.URLAfterProxy == "" || r.RedirectURLOnFail == "" {
		logrus.Error("no cattle info")
		return fmt.Errorf("please input cattle info, accessKey, accessSecret and cattleURL")
	}
	r.CattleTokenURL = c.String("backend-url") + "/v2-beta/token"
	r.rancherAPIClient, err = api.NewRancherClient(c.String("backend-url"), c.String("cattle-access-key"), c.String("cattle-access-secret"))
	if err != nil {
		return err
	}
	return nil
}

func (r *Server) SetCookieHandler(w http.ResponseWriter, req *http.Request) {
	userName := req.URL.Query().Get("userName")
	if userName == "" {
		logrus.Error("userName is empty")
		http.Redirect(w, req, r.RedirectURLOnFail, http.StatusTemporaryRedirect)
		return
	}
	logrus.Infof("userName is %s", userName)

	err := r.CheckUser(userName)
	if err != nil {
		logrus.Error("fail check user", err)
		http.Redirect(w, req, r.RedirectURLOnFail, http.StatusTemporaryRedirect)
		return
	}

	cookies, err := r.GetCookies(userName)
	if err != nil {
		logrus.Error("fail get account", err)
		http.Redirect(w, req, r.RedirectURLOnFail, http.StatusTemporaryRedirect)
		return
	}
	r.SetCookies(w, cookies)
	w.Header().Add("Content-Type", "application/json")

	http.Redirect(w, req, r.URLAfterProxy, http.StatusTemporaryRedirect)
}

func (r *Server) StartServer(port string) {
	http.HandleFunc("/myrancher", http.HandlerFunc(r.SetCookieHandler))
	http.ListenAndServe(port, nil)
}

func (r *Server) CheckUser(userName string) error {
	accountList, err := r.rancherAPIClient.GetAccount()
	if err != nil {
		return err
	}
	for _, v := range accountList {
		if v.Name == userName {
			return nil
		}
	}
	return fmt.Errorf("invalid user")
}

func (r *Server) GetCookies(userName string) (cookies []*http.Cookie, err error) {
	auth := Login{
		Code:         userName + ":" + r.CommonPassword,
		AuthProvider: "localauthconfig",
	}
	authByte, err := json.Marshal(auth)
	if err != nil {
		return nil, err
	}
	payload := bytes.NewReader(authByte)
	req, err := http.NewRequest("POST", r.CattleTokenURL, payload)
	if err != nil {
		return nil, err
	}
	req.Header.Add("content-type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	newObject := AuthResponse{}
	err = json.Unmarshal(body, &newObject)
	logrus.Infof("response from rancher get token, url: %s, response: %s", r.CattleTokenURL, string(body))
	if err != nil {
		return
	}

	c1 := &http.Cookie{
		Name:  "token",
		Value: newObject.Jwt,
	}
	c2 := &http.Cookie{
		Name:  "CSRF",
		Value: "EFBC1899DF",
	}
	c3 := &http.Cookie{
		Name:  "Path",
		Value: "/",
	}
	cookies = append(cookies, c1, c2, c3)
	return cookies, nil
}

func (r *Server) SetCookies(writer http.ResponseWriter, cookies []*http.Cookie) {
	for _, v := range cookies {
		http.SetCookie(writer, v)
	}
}
